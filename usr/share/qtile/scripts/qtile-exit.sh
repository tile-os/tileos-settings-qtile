#!/bin/bash

VARIABLES="DESKTOP_SESSION XDG_CURRENT_DESKTOP XDG_SESSION_DESKTOP XDG_SESSION_TYPE"
VARIABLES="${VARIABLES} DISPLAY WAYLAND_DISPLAY"
SESSION_SHUTDOWN_TARGET="qtile-session-shutdown.target"

session_cleanup () {
    # stop the session target and unset the variables
    systemctl --user start --job-mode=replace-irreversibly "$SESSION_SHUTDOWN_TARGET"
    if [ -n "$VARIABLES" ]; then
        # shellcheck disable=SC2086
        systemctl --user unset-environment $VARIABLES
    fi
}

session_cleanup
qtile cmd-obj -o cmd -f shutdown

