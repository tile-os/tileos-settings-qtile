#!/bin/bash

function send_notification {
local VALUE=$(light -G | cut -d'.' -f1)
local TEXT="Brightness: ${VALUE}%"

notify-send \
    --expire-time 800 \
    --hint string:x-canonical-private-synchronous:brightness \
    --hint "int:value:$VALUE" \
    --hint "int:transient:1" \
    "${TEXT}"
}

case $1 in
    up)
        light -A 5
        send_notification
        ;;
    down)
        light -U 5
        send_notification
        ;;
esac
