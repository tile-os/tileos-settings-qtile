#!/bin/bash

function send_notification {
local VOLUME=$(pulsemixer --get-volume)
# get first percent value
local VOLUME=${VOLUME%%%*}
local VOLUME=${VOLUME##* }

local TEXT="Volume: ${VOLUME}%"
case $(pulsemixer --get-mute) in
     *1)
         TEXT="Volume: muted"
         VOLUME=0
         ;;
esac

notify-send \
    --expire-time 800 \
    --hint string:x-canonical-private-synchronous:volume \
    --hint "int:value:$VOLUME" \
    --hint "int:transient:1" \
    "${TEXT}"
}

case $1 in
    up)
        pulsemixer --change-volume +5
        send_notification
        ;;
    down)
        pulsemixer --change-volume -5
        send_notification
        ;;
    mute)
        pulsemixer --toggle-mute
        send_notification
        ;;
esac
