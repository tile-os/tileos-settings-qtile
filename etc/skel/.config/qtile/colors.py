Macchiato = [
    ["#f4dbd6", "#f4dbd6"], # 0 rosewater
    ["#f0c6c6", "#f0c6c6"], # 1 flamingo
    ["#f5bde6", "#f5bde6"], # 2 pink
    ["#c6a0f6", "#c6a0f6"], # 3 mauve
    ["#ed8796", "#ed8796"], # 4 red
    ["#ee99a0", "#ee99a0"], # 5 maroon
    ["#f5a97f", "#f5a97f"], # 6 peach
    ["#a6da95", "#a6da95"], # 7 green
    ["#8bd5ca", "#8bd5ca"], # 8 teal
    ["#91d7e3", "#91d7e3"], # 9 sky
    ["#7dc4e4", "#7dc4e4"], # 10 saphire
    ["#8aadf4", "#8aadf4"], # 11 blue
    ["#b7bdf8", "#b7bdf8"], # 12 lavender
    ["#cad3f5", "#cad3f5"], # 13 text
    ["#b8c0e0", "#b8c0e0"], # 14 subtext1
    ["#a5adcb", "#a5adcb"], # 15 subtext0
    ["#939ab7", "#939ab7"], # 16 overlay2
    ["#8087a2", "#8087a2"], # 17 overlay1
    ["#6e738d", "#6e738d"], # 18 overlay0
    ["#5b6078", "#5b6078"], # 19 surface2
    ["#494d64", "#494d64"], # 20 surface1
    ["#363a4f", "#363a4f"], # 21 surface0
    ["#24273a", "#24273a"], # 22 base
    ["#1e2030", "#1e2030"], # 23 mantle
    ["#181926", "#181926"] # 24 crust
]