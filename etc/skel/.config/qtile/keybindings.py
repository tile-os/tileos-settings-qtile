# Default keybindings for Qtile on TileOS
# A list of available commands that can be bound to keys can be found
# at https://docs.qtile.org/en/latest/manual/config/lazy.html

from libqtile.config import Key
from libqtile.command import lazy

mod = "mod4"
terminal = "alacritty"

keys = [

    # Main keybindings
    Key(
        [mod],
        "Return",
        lazy.spawn(terminal),
        desc="Launch terminal"
    ),
    Key(
        [mod],
        "Tab",
        lazy.next_layout(),
        desc="Toggle between layouts"
    ),
    Key(
        [mod,"shift"],
        "q",
        lazy.window.kill(),
        desc="Kill focused window"
    ),
    Key(
        [mod, "control"],
        "r",
        lazy.reload_config(),
        desc="Reload the config"
    ),
    Key(
        [mod, "shift"],
        "e",
        lazy.spawn("nwg-bar"),
        desc="Logout menu"
    ),
    Key(
        [mod],
        "r",
        lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"
    ),
    Key([mod], "d",
        lazy.spawn("rofi -show combi -combi-modi 'drun,run' -terminal terminal -ssh-command '{terminal} {ssh-client} {host} [-p {port}]' -run-shell-command '{terminal} {cmd}'"),
        desc="Launch Rofi menu"
    ),
    # Switch between windows
    Key(
        [mod],
        "h",
        lazy.layout.left(),
        desc="Move focus to left"
    ),
    Key(
        [mod],
        "l",
        lazy.layout.right(),
        desc="Move focus to right"
    ),
    Key(
        [mod],
        "j",
        lazy.layout.down(),
        desc="Move focus down"
    ),
    Key(
        [mod],
        "k",
        lazy.layout.up(),
        desc="Move focus up"
    ),

    Key(
        [mod],
        "space",
        lazy.layout.next(),
        desc="Switch window focus to other pane(s) of stack"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"],
        "h",
        lazy.layout.shuffle_left(),
        desc="Move window to the left"
    ),
    Key([mod, "shift"],
        "l",
        lazy.layout.shuffle_right(),
        desc="Move window to the right"
    ),
    Key([mod, "shift"],
        "j",
        lazy.layout.shuffle_down(),
        desc="Move window down"
    ),
    Key(
        [mod, "shift"],
        "k",
        lazy.layout.shuffle_up(),
        desc="Move window up"
    ),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key(
        [mod, "control"],
        "h",
        lazy.layout.grow_left(),
        desc="Grow window to the left"
    ),
    Key(
        [mod, "control"],
        "l",
        lazy.layout.grow_right(),
        desc="Grow window to the right"
    ),
    Key(
        [mod, "control"],
        "j",
        lazy.layout.grow_down(),
        desc="Grow window down"
    ),
    Key(
        [mod, "control"],
        "k",
        lazy.layout.grow_up(),
        desc="Grow window up"
    ),
    Key(
        [mod],
        "n",
        lazy.layout.normalize(),
        desc="Reset all window sizes"
    ),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key(
        [mod],
        "f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen on the focused window",
    ),
    Key(
        [mod, "shift"],
        "space",
        lazy.window.toggle_floating(),
        desc="Toggle floating on the focused window"),

    # Volume
    Key(
        [],
        "XF86AudioMute",
        lazy.spawn("/usr/share/qtile/scripts/volume-control.sh mute"),
        desc='Mute audio'
    ),
    Key(
        [],
        "XF86AudioLowerVolume",
        lazy.spawn("/usr/share/qtile/scripts/volume-control.sh down"),
        desc='Volume down'
    ),
    Key(
        [],
        "XF86AudioRaiseVolume",
        lazy.spawn("/usr/share/qtile/scripts/volume-control.sh up"),
        desc='Volume up'
    ),

    # Brightness
    Key(
        [],
        "XF86MonBrightnessDown",
        lazy.spawn("/usr/share/qtile/scripts/brightness-control.sh down"),
        desc='Brightness down'
    ),
    Key(
        [],
        "XF86MonBrightnessUp",
        lazy.spawn("/usr/share/qtile/scripts/brightness-control.sh up"),
        desc='Brightness up'
    ),

    # Screenshots
    # Take a screenshot of the currently focused output and save it into screenshots
    Key(
        [],
        "Print",
        lazy.spawn("/usr/share/qtile/scripts/screenshot.sh"),
        desc='Save the screens of the currently focused output to the screenshots folder'
    ),
    # Take a screenshot of the selected region
    Key(
        [mod],
        "Print",
        lazy.spawn("/usr/share/qtile/scripts/screenshot.sh selected-region"),
        desc='Save the selected region of the screen to the screenshots folder'
    ),
    # Capture region of screen to clipboard
    Key(
        [mod,"shift"],
        "Print",
        lazy.spawn("/usr/share/qtile/scripts/screenshot.sh save-to-clipboard"),
        desc='Capture a region of the screen to the clipboard'
    ),
    # Take a screenshot of the selected window
    Key(
        [mod, "control"],
        "Print",
        lazy.spawn("/usr/share/qtile/scripts/screenshot.sh focused-window"),
        desc='Save the selected window to the screenshots folder'
    ),

] 
