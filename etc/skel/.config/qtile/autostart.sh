#!/bin/bash

export XDG_CURRENT_DESKTOP=wlroots
export XDG_SESSION_DESKTOP="${XDG_SESSION_DESKTOP:-wlroots}"
export XDG_SESSION_TYPE=wayland
VARIABLES="DESKTOP_SESSION XDG_CURRENT_DESKTOP XDG_SESSION_DESKTOP XDG_SESSION_TYPE"
VARIABLES="${VARIABLES} DISPLAY WAYLAND_DISPLAY"
SESSION_TARGET="qtile-session.target"
SESSION_XDG_AUTOSTART_TARGET="qtile-xdg-autostart.target"
SNI_CHECK="/usr/share/qtile/scripts/wait-sni-ready"

# shellcheck disable=SC2086
dbus-update-activation-environment --systemd ${VARIABLES:- --all} &

# reset failed state of all user units
systemctl --user reset-failed

# shellcheck disable=SC2086
systemctl --user import-environment $VARIABLES
systemctl --user start "$SESSION_TARGET"

# Wait for StatusNotifierWatcher is available and start XDG Autostart target
"$SNI_CHECK" && systemctl --user start "$SESSION_XDG_AUTOSTART_TARGET"

# PolicyKit Agent
/usr/bin/mate-polkit

# Wallpaper script (you can change wallpaper with Azote)
source ~/.azotebg

# Start nwg-drawer as a daemon
pkill nwg-drawer
nwg-drawer -r &

# Start PCmanFM-Qt as a daemon
pkill pcmanfm-qt
pcmanfm-qt -d &

# Clipboard daemon
pkill wl-paste
wl-paste --watch cliphist store &